##%%
import numpy as np
import cv2 as cv
import os
import pathlib

##%%
'''parametros default de las camaras logitech c270 hd webcam'''
def_params_cam={'exp':-6.0,'bri':128.0,'cont':32.0,'sat':32.0,'gain':0.0}
start_time = cv.getTickCount()
cam1=cv.VideoCapture(0)
end_time = cv.getTickCount()
computation_time = (end_time - start_time) / cv.getTickFrequency()
print(computation_time)

## %%
'''Set exposure mode to manual and backlight compensation to off'''
# cam1.set(cv.CAP_PROP_AUTO_EXPOSURE,-1)
# print(cam1.get(cv.CAP_PROP_AUTO_EXPOSURE))
# #%%
# cam1.set(cv.CAP_PROP_BACKLIGHT,1)
# print(cam1.get(cv.CAP_PROP_BACKLIGHT))
# #%%
# cam1.set(cv.CAP_PROP_AUTO_EXPOSURE,0)
# cam1.set(cv.CAP_PROP_BACKLIGHT,0)

##%%
# cam2=cv.VideoCapture(2)
# cam2.set(cv.CAP_PROP_AUTO_EXPOSURE,1)
# cam2.set(cv.CAP_PROP_BACKLIGHT,1)

# def shoot(cam:int,fname:str,exp=-6.0,gain=0.0,directory=r'C:\Users\alima\OneDrive\Estudios\Laboratorio 6\codigo\fotos\carac cam',bri=128.0,cont=32.0,sat=32.0):
#     '''funcion para setear parametros y sacar foto con una camara \n \n Params: \n cam:indice de la camara a usar \n exp: exposicion (default -6.0, entre -13.0 y 0) \n gan: ganancia (default=0.0) \n bri:brillo (default=128.0) \n cont: contraste (default=32.0) \n sat: saturacion (default=32.0)'''
#     cap=cv.VideoCapture(cam)
#     if not cap.isOpened():
#           return print('No se pudo abrir la camara')
#     if bri!=128.0:
#         cap.set(cv.CAP_PROP_BRIGHTNESS,bri)
#         bri=cap.get(cv.CAP_PROP_BRIGHTNESS)

#     if cont!=32.0:
#         cap.set(cv.CAP_PROP_CONTRAST,cont)
#         cont=cap.get(cv.CAP_PROP_CONTRAST)

#     if sat!=32.0:
#         cap.set(cv.CAP_PROP_SATURATION,sat)  
#         sat=cap.get(cv.CAP_PROP_SATURATION)

#     if gain!=0.0:
#         cap.set(cv.CAP_PROP_GAIN,gain) 
#         gain=cap.get(cv.CAP_PROP_GAIN)

#     if exp!=-6.0:
#         cap.set(cv.CAP_PROP_EXPOSURE,exp)
#         exp=cap.get(cv.CAP_PROP_EXPOSURE)


#     ret,img=cap.read()
#     cv.imshow(f'exp {exp}, gain {gain} ',img)
#     os.chdir(directory)
#     stat=cv.imwrite(fname,img)
#     cv.waitKey(0)
    
#     bri=cap.get(cv.CAP_PROP_BRIGHTNESS)
#     cont=cap.get(cv.CAP_PROP_CONTRAST)
#     sat=cap.get(cv.CAP_PROP_SATURATION)
#     gain=cap.get(cv.CAP_PROP_GAIN)
#     exp=cap.get(cv.CAP_PROP_EXPOSURE)
#     return print(stat)



def barrido_exp(cam:int,fname:str,gain=0.0,directory=r'C:\Users\alima\OneDrive\Estudios\Laboratorio 6\codigo\fotos\carac cam'):
    '''funcion para setear parametros y sacar foto con una camara \n \n Params: \n cam:indice de la camara a usar \n exp: exposicion (default -6.0, entre -13.0 y 0) \n gan: ganancia (default=0.0) \n bri:brillo (default=128.0) \n cont: contraste (default=32.0) \n sat: saturacion (default=32.0)'''
    os.chdir(directory)
    cap=cv.VideoCapture(cam)
    start_time = cv.getTickCount()
    if not cap.isOpened():
          return print('No se pudo abrir la camara')
    if gain!=0.0:
        cap.set(cv.CAP_PROP_GAIN,gain) 
        gain=cap.get(cv.CAP_PROP_GAIN)


    exp=np.arange(-13,0,1)
    imgs=np.array([])
    for i in exp:
        cap.set(cv.CAP_PROP_EXPOSURE,i)
        #np.append(imgs,cap.read()[2])
        ret,img=cap.read()
        cv.imwrite(fname+f'_exp{i}.png',img)
    cv.destroyAllWindows
    end_time = cv.getTickCount()
    computation_time = (end_time - start_time) / cv.getTickFrequency()
    print(computation_time)



def barrido_gain(cam:int,fname:str,exp=-6.0,directory=r'C:\Users\alima\OneDrive\Estudios\Laboratorio 6\codigo\fotos\carac cam'):
    '''funcion para setear parametros y sacar foto con una camara \n \n Params: \n cam:indice de la camara a usar \n exp: exposicion (default -6.0, entre -13.0 y 0) \n gan: ganancia (default=0.0) \n bri:brillo (default=128.0) \n cont: contraste (default=32.0) \n sat: saturacion (default=32.0)'''
    os.chdir(directory)
    start_time = cv.getTickCount()
    cap=cv.VideoCapture(cam)
    if not cap.isOpened():
          return print('No se pudo abrir la camara')
    if exp!=-6.0:
        cap.set(cv.CAP_PROP_EXPOSURE,exp) 
        gain=cap.get(cv.CAP_PROP_EXPOSURE)

    
    gain=np.arange(0,250,10)
    imgs=np.array([])
    for i in gain:
        cap.set(cv.CAP_PROP_GAIN,i)
        #np.append(imgs,cap.read()[2])
        ret,img=cap.read()
        cv.imwrite(fname+f'_gain{i}.png',img)
    cv.destroyAllWindows
    end_time = cv.getTickCount()
    computation_time = (end_time - start_time) / cv.getTickFrequency()
    print(computation_time)


def barrido(cam,fname:str,directory=r'C:\Users\alima\OneDrive\Estudios\Laboratorio 6\codigo\fotos\carac om179'):
    '''funcion para setear parametros y sacar foto con una camara \n \n Params: \n cam:indice de la camara a usar \n exp: exposicion (default -6.0, entre -13.0 y 0) \n gan: ganancia (default=0.0) \n bri:brillo (default=128.0) \n cont: contraste (default=32.0) \n sat: saturacion (default=32.0)'''  
    gain=np.arange(0,300,50)
    exp=np.arange(-13,1,1)
    for i in gain:
        cam.set(cv.CAP_PROP_GAIN,i)
        cv.waitKey(100)  
        for j in exp:
            cam.set(cv.CAP_PROP_EXPOSURE,j)  
            cv.waitKey(100)  
            directory=rf'C:\Users\alima\OneDrive\Estudios\Laboratorio 6\codigo\fotos\carac om179\exp{j}_gain{i}'
            pathlib.Path(directory).mkdir(parents=True, exist_ok=True)
            os.chdir(directory)
            ret,img=cam.read()
            cv.imwrite(fname+f'exp{j}_gain{i}.png',img)
        cv.destroyAllWindows


def video(cam):
    ret = True
    while ret:
        # Capture frame-by-frame
        ret, frame = cam.read()
        # if frame is read correctly ret is True
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            if cv.waitKey(0)==ord('q'):
                break
        # Our operations on the frame come here
        #gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        # Display the resulting frame
        cv.imshow('video', frame)
        if cv.waitKey(1) == ord('q'):
            ret = False
    # When everything done, release the capture
    cv.destroyAllWindows()

def ruido(cam,exp,gain,corr,directory=r'C:\Users\alima\OneDrive\Estudios\Laboratorio 6\codigo\fotos\carac om179\ruido'):
    os.chdir(directory)
    cam.set(cv.CAP_PROP_EXPOSURE,exp)
    cv.waitKey(500)
    cam.set(cv.CAP_PROP_GAIN,gain)
    cv.waitKey(500)
    for i in range(1,101):
        ret, img = cam.read()
        if not ret:
            print(f"Fallo en la foto numero {i}")
            break
        directory=rf'C:\Users\alima\OneDrive\Estudios\Laboratorio 6\codigo\fotos\carac om179\ruido\ruido exp{exp} gain{gain}'
        pathlib.Path(directory).mkdir(parents=True, exist_ok=True)
        os.chdir(directory)
        cv.imwrite(f'om180_ruido{i}_{corr}mA_exp{exp}_gain{gain}.png',img)
        print(f"Foto {i} sacada correctamente")
        cv.destroyAllWindows
    cv.destroyAllWindows
    
# #%%
# video(cam1)

# #%%
# corr=input("Corriente sobre el LED?: ")
# barrido(cam1,f"om179_{corr}mA")

# # %%
# corr=input("Corriente sobre el LED? ")
# for exp in [-6,-8,-10,-11,-12,-13]:
#     for gain in [0,50,100,150,200]:
#         ruido(cam1,exp,gain,corr)

# # %%
# os.chdir(r"C:\Users\alima\OneDrive\Estudios\Laboratorio 6\codigo\fotos\carac om179")
# # %%
