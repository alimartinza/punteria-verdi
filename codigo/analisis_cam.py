import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt

def color_split(fotopath:str):
    img=cv.imread(fotopath)
    cv.imshow(fotopath,"foto")
    blue,green,red=cv.split(img) #separar los canales de la imagen
    zeros=np.zeros(blue.shape,np.uint8) #hacemos una matriz de zeros del tamaño correcto, y le ponemos de argunemnto uni8 que es unasigned 8bit integer
    #creamos 3 imagenes separadas con cada canal, mergeando 0´s en los canales que no son el correcto
    blueBGR=cv.merge((blue,zeros,zeros))
    greenBGR=cv.merge((zeros,green,zeros))
    redBGR=cv.merge((zeros,zeros,red))
    cv.destroyAllWindows()
    return blueBGR,greenBGR,redBGR

def gray_split(fotopath:str):
    img=cv.imread(fotopath)
    cv.imshow(fotopath,"foto")
    blue,green,red=cv.split(img) #separamos los canales de la imagen, nos devuelve 3 imagenes en greyscale de cada canal
    cv.destroyAllWindows()
    return blue,green,red

def rect(fotopath,x0:list,x1:list):
    '''Elegi una foto en el path "fotopath" y dos vertices x0 y x1 para hacer un rectangulo sobre la imagen, sirve para seleccionar
    el ROI de la imagen que queres analizar'''
    img=cv.imread(fotopath)
    cv.rectangle(img,x0,x1,(0, 200, 0),3)
    cv.imshow("Seleccion del rectangulo",img)
    cv.waitKey(0)
    
def hist_bgr(fotopath,roi:list): #mat="imagen" roi=((325,260),(375,310))
    '''
    Plot de histogramas para greyscale, blue, red y green dada una imagen.
    Params:
    -fotopath: path a la foto a analizar
    -roi: parte de la imagen a analizar, podemos usar la funcion rect para ver que roi estamos seleccionando, tiene que ser de la forma 
    (x,y) donde x=(,),y=(,)
    Returns: 
    -printed histogramas de b,g,r y greyscale
    -arr: array containing mean values and std's of the three colour channels and the greyscale image
    '''
    mat=cv.imread(fotopath)
    mat=mat[roi[0][0]:roi[1][0],roi[0][1]:roi[1][1]]

    grey=cv.cvtColor(mat, cv.COLOR_BGR2GRAY)
    blue,green,red=cv.split(mat) #separar los canales de la imagen
    cv.destroyAllWindows()

    b_mean=np.mean(blue)
    b_std=np.std(blue)
    # plt.title('Blue',loc='center',fontsize=18)
    # plt.hist(blue.flatten(),bins=np.arange(np.amin(blue),np.amax(blue)+1)+0.5, ec="k")
    # plt.vlines(b_mean,ymin=0,ymax=500,color='grey',linestyles='dashed')
    # plt.show()

    g_mean=np.mean(green)
    g_std=np.std(green)
    # plt.title('Green',loc='center',fontsize=18)
    # plt.vlines(g_mean,ymin=0,ymax=500,color='grey',linestyles='dashed')
    # plt.hist(green.flatten(),bins=np.arange(np.amin(green),np.amax(green)+1)+0.5, ec="k")
    # plt.show()

    red=red.flatten()
    r_mean=np.mean(red)
    r_std=np.std(red)
    # plt.title('Red',loc='center',fontsize=18)
    # plt.vlines(r_mean,ymin=0,ymax=500,color='grey',linestyles='dashed')
    # plt.hist(red,bins=np.arange(np.amin(red),np.amax(red)+1)+0.5, ec="k")
    # plt.show()
    
    grey_mean=np.mean(grey)
    grey_std=np.std(grey)
    # plt.title('Red',loc='center',fontsize=18)
    # plt.vlines(grey_mean,ymin=0,ymax=500,color='grey',linestyles='dashed')
    # plt.hist(grey.flatten(),bins=np.arange(np.amin(grey),np.amax(grey)+1)+0.5, ec="k")
    # plt.show()

    mean=[b_mean,g_mean,r_mean,grey_mean]
    std=[b_std,g_std,r_std,grey_std]

    return mean,std

cam=["om179","om180"]
I=[0.22,0.44,0.61,0.66,0.81,1.00,1.01,1.22,1.53,1.73,2.08,2.29]
x0=(325,260)
x1=(x0[0]+50,x0[1]+50)


#rect(r"codigo\fotos\carac om179\exp-10_gain0\om179_0.61mAexp-10_gain0.png",x0,x1)
expo=[12,11,10,9,8,7,6,5,4,3,2,1]
gain=[0,50,100,150,200,250]


for exp in expo: 
    for ga in gain:
        res=[[],[],[],[]]
        err=[[],[],[],[]]
        curr=["0.22","0.40","0.61","0.66","0.81","1.00","1.01","1.22","1.53","1.73","2.08","2.29"]
        for i in curr:
            x,y=hist_bgr(rf"C:\Users\alima\Laboratorio 6\codigo\fotos\carac om179\exp-{exp}_gain{ga}\om179_"+i+f"mAexp-{exp}_gain{ga}.png",(x0,x1))
            for j in range(4):
                res[j].append(x[j])
                err[j].append(y[j])

        print(f"Comenzando con exp-{exp} gain{ga}")
        curr=[0.22,0.40,0.61,0.66,0.81,1.00,1.01,1.22,1.53,1.73,2.08,2.29]
            
        plt.title(f'om179 exp{exp} gain{ga}',fontsize=20)
        plt.xlabel("I [mA]",fontsize=20)
        plt.ylabel("Intensidad [U.A.]",fontsize=20)
        plt.tick_params("both",size=18)
        plt.xlim(0,2.3)
        plt.ylim(0,256)
        plt.errorbar(curr,res[0],yerr=err[0],xerr=0.01,color='blue',label='Canal azul')
        plt.errorbar(curr,res[1],yerr=err[1],xerr=0.01,color='green',label='Canal verde')
        plt.errorbar(curr,res[2],yerr=err[2],xerr=0.01,color='red',label='Canal rojo')
        plt.errorbar(curr,res[3],yerr=err[3],xerr=0.01,color='black',label='Greyscale')
        plt.tight_layout()
        plt.legend()
        plt.show()
        plt.savefig(rf"C:\Users\alima\Laboratorio 6\codigo\graficos\om 179\om179_exp{exp}_gain{ga}", dpi='figure', 
                format=None, metadata=None,
                bbox_inches=None, pad_inches=0.1,
                facecolor='auto', edgecolor='auto',
                backend=None
                )
        print(f"Terminado con exp-{exp} gain{ga}")
        

