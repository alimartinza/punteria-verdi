const int voltagePin = A0;  // Analog input pin to measure voltage
const int resistance = 996;
void setup() {
  Serial.begin(9600);  // Initialize serial communication
}

void loop() {
  // Read the voltage value from the analog input pin
  int rawValue = analogRead(voltagePin);

  // Convert the raw ADC value to voltage
  float voltage = rawValue * (5.0 / 1023.0);

  // Print the voltage value
  Serial.print("Current: ");
  Serial.print(1000*voltage/resistance, 2);  // Print with 2 decimal places
  Serial.println(" mA");

  delay(3000);  // Delay between measurements (adjust as needed)
}


