"""Script de prueba"""
import numpy as np
import cv2 as cv

sat=32.0
gain=0.0
exp=-6.0
#parametros default de las camaras logitech c270 hd webcam
def_params_cam={'sat':32.0,}

#funcion para setear parametros y sacar foto
def setshoot(cam:int,exp=-6.0,bri=128.0,cont=32.0,sat=32.0,gain=0.0):
    '''funcion para setear parametros y sacar foto con una camara \n \n Params: \n cam:indice de la camara a usar \n exp: exposicion (default -6.0, entre -13.0 y 0) \n gan: ganancia (default=0.0) \n bri:brillo (default=128.0) \n cont: contraste (default=32.0) \n sat: saturacion (default=32.0)'''
    cap=cv.VideoCapture(cam)
    if not cap.isOpened():
          return print('No se pudo abrir la camara')
    cap.set(cv.CAP_PROP_BRIGHTNESS,bri)
    cap.set(cv.CAP_PROP_CONTRAST,cont)
    cap.set(cv.CAP_PROP_SATURATION,sat)
    cap.set(cv.CAP_PROP_GAIN,gain)
    cap.set(cv.CAP_PROP_EXPOSURE,exp)

    bri=cap.get(cv.CAP_PROP_BRIGHTNESS)
    cont=cap.get(cv.CAP_PROP_CONTRAST)
    sat=cap.get(cv.CAP_PROP_SATURATION)
    gain=cap.get(cv.CAP_PROP_GAIN)
    exp=cap.get(cv.CAP_PROP_EXPOSURE)

    ret, frame=cap.read()
    #cv.waitKey(1)
    
    bri=cap.get(cv.CAP_PROP_BRIGHTNESS)
    cont=cap.get(cv.CAP_PROP_CONTRAST)
    sat=cap.get(cv.CAP_PROP_SATURATION)
    gain=cap.get(cv.CAP_PROP_GAIN)
    exp=cap.get(cv.CAP_PROP_EXPOSURE)
    return print('Parametros finales:')

cap = cv.VideoCapture(2)
if not cap.isOpened():
    print("Cannot open camara")
    exit()
if cap.isOpened():
	print("Camara opened correctly")


mode=cap.get(cv.CAP_PROP_MODE)
bri=cap.get(cv.CAP_PROP_BRIGHTNESS)
cont=cap.get(cv.CAP_PROP_CONTRAST)
sat=cap.get(cv.CAP_PROP_SATURATION)
gain=cap.get(cv.CAP_PROP_GAIN)
exp=cap.get(cv.CAP_PROP_EXPOSURE)
print(f'exp time antes {exp}')
expset=cap.set(cv.CAP_PROP_EXPOSURE,-0)
exp=cap.get(cv.CAP_PROP_EXPOSURE)
print('Se puede cambiar el exp time?',expset,'\n','Nuevo exp time=',exp)

gamma=cap.get(cv.CAP_PROP_GAMMA)
print(f'Gamma antes {gamma}')
gammaset=cap.set(cv.CAP_PROP_GAMMA,90.0)
gamma=cap.get(cv.CAP_PROP_GAMMA)
print(f'Se puede cambiar el gamma? {gammaset}')
print(f'Gamma despues {gamma}')

print(f'mode= {mode}',f'Brillo {bri}',f"Contraste {cont}",f"Saturacion {sat}",f"Ganancia {gain}",f"Tiempo de exposicion {exp}",f"Gamma {gamma}",sep="\n")

ret, frame=cap.read()

if not ret:
	print("No se puede leer el frame correctamente")

if ret:
	print("Se puede leer el frame correctamente")
	cv.imshow("frame",frame)

cv.waitKey(10)

while True:
    # Capture frame-by-frame
    ret, frame = cap.read()
    # if frame is read correctly ret is True
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break
    # Display the resulting frame
    cv.imshow('video', frame)
    if cv.waitKey(1) == ord('q'):
        break
# When everything done, release the capture
cap.release()
cv.destroyAllWindows()
	
