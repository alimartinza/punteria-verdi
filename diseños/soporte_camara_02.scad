$fa = 2;
$fs = 0.2;
color("grey"){


difference(){
    union() {
    cube([14.0,14.0,4.8],center=true);
    translate([0,0,5.9])
        cylinder(h=7.0,r=22.8/2,center=true);
    translate([0,0,2.4+7+2.5])
        cylinder(h=5.0,r=25.4/2,center=true);
    }
    translate([0,0,9.5])
        cylinder(h=24,r=4,center=true);
    translate([0,0,-1])
        cube([8.0,8.0,3],center=true);
    }


translate([8.5,0,0])
    cylinder(h=4.8,r=2.25,center=true);
translate([-8.5,0,0])
    cylinder(h=4.8,r=2.25,center=true);


}
