import cv2
import numpy as np

# Define the lower and upper bounds for the red color
lower_red = np.array([0, 50, 50])  # Modify these values as per your requirements
upper_red = np.array([10, 255, 255])  # Modify these values as per your requirements

# Open the webcam
# Start the computation time
start_time = cv2.getTickCount()
cap = cv2.VideoCapture(0)
end_time = cv2.getTickCount()
computation_time = (end_time - start_time) / cv2.getTickFrequency()
print("Computation Time opening camera: %.2fms" % (computation_time * 1000))

# Check if the webcam is opened successfully
if not cap.isOpened():
    print("Failed to open the webcam")
    exit()

while True:
    # Start the computation time
    start_time = cv2.getTickCount()
    ret, frame = cap.read()

    if not ret:
        print("Failed to read frame from the webcam")
        break

    # Convert the frame to the HSV color space
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # Threshold the HSV image to get only the red color
    mask = cv2.inRange(hsv, lower_red, upper_red)

    # Find contours of the red objects in the thresholded image
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Draw rectangles and compute center coordinates of the detected red objects
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

        # Calculate the center coordinates
        center_x = x + (w/2)
        center_y = y + (h/2)

        # Draw a circle at the center coordinates
        cv2.circle(frame, (center_x, center_y), 4, (0, 0, 255), -1)

        # Print the center coordinates
        print("Center coordinates: (x=%d, y=%d)" % (center_x, center_y))


    end_time = cv2.getTickCount()
    computation_time = (end_time - start_time) / cv2.getTickFrequency()
    # Add a text box to the frame with computation time
    message = "%.2fms" % (computation_time * 1000)
    text_position = (10, 30)
    font = cv2.FONT_HERSHEY_SIMPLEX
    font_scale = 1
    color = (0, 255, 0)
    thickness = 2
    cv2.putText(frame, message, text_position, font, font_scale, color, thickness)

    # Display the frame with the detected red objects
    cv2.imshow("Green Spot Tracking", frame)

    # Exit the loop if 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release the webcam and close any open windows
cap.release()
cv2.destroyAllWindows()
