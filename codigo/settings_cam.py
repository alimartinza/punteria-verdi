#%%
import cv2 as cv
import numpy as np
from multiprocessing import Pool
import time

def video(cam):
    print(f"Abriendo la camara {cam}")

    cap = cv.VideoCapture(cam)
    if not cap.isOpened():
        print(f'No se pudo abrir la camara {cam}')
    
    exp = -13
    gain = 0

    time.sleep(2)
    print(f"cambiando la exposure de camara {cam} a {exp}")
    cap.set(cv.CAP_PROP_EXPOSURE,exp)
    time.sleep(2)
    print(f"Cambiando el gain de la camara {cam} a {gain}")
    cap.set(cv.CAP_PROP_GAIN,gain)
    time.sleep(2)

    while True:

        # Capture frame-by-frame
        ret, frame = cap.read()
        # if frame is read correctly ret is True
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            if cv.waitKey(0)==ord('q'):
                break
        # Our operations on the frame come here
        #gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        # Display the resulting frame
        cv.imshow('Video ' + str(cam), frame)
        if cv.waitKey(1) == ord('q'):
            break
    # When everything done, release the capture
    cap.release()
    cv.destroyAllWindows()


if __name__ == '__main__':
    pool = Pool(processes=2)
    vid0 , vid1 = pool.map(video , [0,1])


